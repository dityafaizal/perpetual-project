module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'blue': '#0ac0cb',
        'whiteBlue': '#EEF3F4',
        'darkGrey': '#231f20',
        'mediumGrey': '#354E57',
        'lightGrey': '#9ca3af',
        'lightGrey20': 'rgba(156, 163, 175, .2)',
        'red': '#ff0c00'
      }
    },
    fontFamily: {
      'proximaLight': ['Milliard Light'],
      'proximaMedium': ['Milliard Medium'],
      'proximaSemibold': ['Milliard SemiBold'],
      'proximaBold': ['Milliard Bold']
    }
  },
  plugins: [],
}
